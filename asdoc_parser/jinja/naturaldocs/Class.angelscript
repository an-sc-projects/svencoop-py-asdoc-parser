{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Property.angelscript" as Property %}
{% import "Method.angelscript" as Method %}
{% import "Common.angelscript" as Common %}
{% set _className = root.ClassName %}
{% set _classNamespace = Namespace.prefixNamespace(root.Namespace) + _className %}
{% set _classNamespace2 = Namespace.prefixNamespaceNoGlobal(root.Namespace) + _className %}
/*
 * class: {{ _classNamespace }}
 * --- C++
 * class {{ _classNamespace2 }} {...}
 * ---
 * {{ Common.fixNewline( root.Documentation ) }}
 */

{{ Method.getMethods( root.Methods ) }}

{{ Property.getProperties( root.Properties ) }}
