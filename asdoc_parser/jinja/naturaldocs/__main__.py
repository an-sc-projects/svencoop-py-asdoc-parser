import sys
import time
from pathlib import Path
import asdoc_parser as ASDoc
from jinja2 import Environment, PackageLoader


ND_PATH = './nd'
SRC_PATH = './src'
HTML_PATH = './html'


env = Environment(
    loader=PackageLoader( 'asdoc_parser.jinja', 'naturaldocs' )
    , autoescape=False
)


def WriteOutput( templateName, templateDict, outputPath ):
    outputDir = (Path(outputPath) / '..').resolve()
    outputDir.mkdir( parents=True, exist_ok=True )
    template = env.get_template( templateName )
    #print( template.render(templateDict) )
    # Function/method overloads
    if Path(outputPath).exists(): print( "Overload:", outputPath )
    oPath = Path(outputPath)
    oName = oPath.stem
    oExt  = oPath.suffix
    variant = 2
    while oPath.exists():
        oPath = outputDir / f"{oName}@{(variant)}{oExt}"
        variant += 1
    outputPath = oPath.resolve().as_posix()
    template.stream( templateDict ).dump( outputPath )


def WriteNDConfig( rootDict, outputPath ):
    # Project.txt
    title = (tuple(rootDict.keys()))[-1]
    rootDict = rootDict[title]
    templateDict = {
        'root': rootDict
        , 'title': title
        , 'src_path': '../' + SRC_PATH
        , 'html_path': '../' + HTML_PATH
    }
    WriteOutput( 'Project.ndconfig', templateDict, (Path(outputPath) / ND_PATH / 'Project.txt' ).resolve().as_posix() );
    # Languages.txt
    templateDict = {
        'root': rootDict
    }
    WriteOutput( 'Languages.ndconfig', templateDict, (Path(outputPath) / ND_PATH / 'Languages.txt' ).resolve().as_posix() );
    # Comments.txt
    templateDict = {
        'root': rootDict
    }
    WriteOutput( 'Comments.ndconfig', templateDict, (Path(outputPath) / ND_PATH / 'Comments.txt' ).resolve().as_posix() );


def WriteAngelscript( rootDict, arrayName, outputPath, filenameKey = "Name" ):
    outputPath = (Path(outputPath) / arrayName).resolve()
    for item in reversed(rootDict[arrayName]):
        itemName = (tuple(item.keys()))[-1]
        root = item[itemName]
        # filename.as
        # Special cases
        if itemName == "Function":
            fileName = getFunction(root["Declaration"])
        elif arrayName == "Properties":
            fileName = getVariable(root["Declaration"])
        elif itemName == "FuncDef":
            fileName = getFunction(root["Name"])
        else:
            fileName = root[filenameKey]
        templateDict = {
            'root': root
        }
        WriteOutput( f'{itemName}.angelscript', templateDict, (Path(outputPath) / f'{fileName}.as' ).resolve().as_posix() );


def getFunction(function):
    return function.split('(')[0].split(' ')[-1]


def getVariable(variable):
    return variable.split(' ')[-1]


if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        rootDict = ASDoc.parse(f.read())
        rootKey = (tuple(rootDict.keys()))[-1];
        outputPath = Path( rootKey )
        if outputPath.exists():
            outputPath = Path( rootKey + time.strftime("_%Y%m%d_%H%M%S") )
        # NaturalDocs Configurations
        WriteNDConfig( rootDict, outputPath )
        # HTML Directory
        (outputPath / HTML_PATH).mkdir( parents=True, exist_ok=True )
        # Angelscripts
        rootDict = rootDict[rootKey]
        outputPath /= SRC_PATH
        WriteAngelscript( rootDict, "Interfaces", outputPath, "InterfaceName" )
        WriteAngelscript( rootDict, "Classes", outputPath, "ClassName" )
        WriteAngelscript( rootDict, "Enums", outputPath )
        WriteAngelscript( rootDict, "Functions", outputPath )
        WriteAngelscript( rootDict, "Properties", outputPath )
        WriteAngelscript( rootDict, "Typedefs", outputPath )
        WriteAngelscript( rootDict, "FuncDefs", outputPath )
