{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Method.angelscript" as Method %}
{% import "Common.angelscript" as Common %}
{% set _interfaceName = root.InterfaceName %}
{% set _interfaceNamespace = Namespace.prefixNamespace(root.Namespace) + _interfaceName %}
{% set _interfaceNamespace2 = Namespace.prefixNamespaceNoGlobal(root.Namespace) + _interfaceName %}
/*
 * interface: {{ _interfaceNamespace }}
 * --- C++
 * interface {{ _interfaceNamespace2 }} {...}
 * ---
 * {{ Common.fixNewline( root.Documentation ) }}
 */

{{ Method.getMethods( root.Methods ) }}
