{% set _globalNamespace = "[Global Namespace]" %}
{% macro getNamespace(namespace) -%}
{% if not namespace %}{% set namespace = _globalNamespace %}{% endif %}
/*
 * namespace: {{ namespace }}
 * {% if namespace != _globalNamespace %}--- C++
 * namespace {{ namespace }} {...}
 * ---{% else %}Global namespace.{% endif %}
 */
{%- endmacro %}
{% macro prefixNamespace(namespace) -%}
{% if not namespace %}{% set namespace = _globalNamespace %}{% endif %}{{namespace}}::
{%- endmacro %}
{% macro prefixNamespaceNoGlobal(namespace) -%}
{% if namespace %}{{namespace}}::{% endif %}
{%- endmacro %}