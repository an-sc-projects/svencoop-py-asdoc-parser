{% import "Common.angelscript" as Common %}
{% import "Namespace.angelscript" as Namespace %}
{% macro getProperty(property) -%}
{% set _propertyName = Common.getVariable(property.Declaration) %}
{% set __split = property.Declaration.split(' ') %}
{% set __temp = __split.pop() %}
{% set __temp =__split.append( Namespace.prefixNamespaceNoGlobal(property.Namespace) + _propertyName ) %}
{% set _propertyNamespace = __split|join(' ') %}
/*
 * variable: {{ _propertyName }}
 * {{ Common.fixNewline( property.Documentation ) }}
 */
{{ _propertyNamespace }};
{%- endmacro %}
{% macro getProperties(array) -%}
{% for block in array|reverse %}
{{ getProperty( block.Property ) }}
{% endfor %}
{%- endmacro %}
{% if root %}
{{Namespace.getNamespace(root.Namespace)}}
{{getProperty(root)}}
{% endif %}
