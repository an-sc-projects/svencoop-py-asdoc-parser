{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Common.angelscript" as Common %}
{% set _typedefName = root.Name %}
{% set _typedefNamespace = Namespace.prefixNamespaceNoGlobal(root.Namespace) + _typedefName %}
/*
 * typedef: {{ _typedefName }}
 * --- C++
 * typedef {{ root.Type }} {{ _typedefNamespace }};
 * ---
 * {{ Common.fixNewline( root.Documentation ) }}
 */