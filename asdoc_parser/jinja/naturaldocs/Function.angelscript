{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Common.angelscript" as Common %}
{% set _functionName = Common.getFunction(root.Declaration) %}
{% set _functionNamespace = root.Declaration | replace( _functionName + "(", Namespace.prefixNamespaceNoGlobal(root.Namespace) + _functionName + "(", 1 ) %}
/*
 * function: {{ _functionName }}
 * {{ Common.fixNewline( root.Documentation ) }}
 */
{{ _functionNamespace }} {}