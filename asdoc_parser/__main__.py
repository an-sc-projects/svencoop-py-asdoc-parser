import sys
from .asdoc_parser import parse

with open(sys.argv[1]) as f:
    import json; print(json.dumps(parse(f.read()), indent=2))
    #print(parse(f.read()))
