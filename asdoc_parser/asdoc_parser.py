"""
ASDoc Parser
Created by Anggara_nothing.
Update Time: 28/12/2020
==================

Based on Lark-parser's Simple JSON Parser sample script.
"""
from lark import Lark, Transformer, v_args


GRAMMAR = r"""
    ?start          : string block

    ?value          : string
                    | block

    block           : "{" keyvalue* "}"
    keyvalue        : string value

    string          : STRING

    SYMBOL          : "!" | "#".."/" | ":".."@" | "[".."`" | "|" | "~"
    STRING          : ESCAPED_STRING
                    | (LETTER | DIGIT | SYMBOL)+

    %import common.LETTER
    %import common.ESCAPED_STRING
    %import common.DIGIT

    %import common.WS
    %ignore WS
"""


class ASDocTransformer(Transformer):
    start = lambda self, c: { c[0]: c[1] }
    string = v_args(inline=True)(lambda self, s: s.replace('\\"', '"').replace("\\'", "'").strip("'\""))
    keyvalue = tuple

    def block(self, c):
        try:
            r = c
            # Only has single block item.
            if len(c) == 1:
                if type(c[-1]) is tuple and type(c[-1][-1]) is dict:
                    r = [dict(c)]
            elif len(c) > 1:
                r = dict(c)
                # Not match size.
                if len(r) != len(c):
                    r = [dict([cc]) for cc in c]
            #print("---", c)
            #print("===", r)
            return r
        except Exception as ex:
            print(c, ex)
        return c


Parser = Lark(GRAMMAR,
                   parser='lalr',
                   # Using the standard lexer isn't required, and isn't usually recommended.
                   # But, it's good enough, and it's slightly faster.
                   lexer='standard',
                   # Disabling propagate_positions and placeholders slightly improves speed
                   propagate_positions=False,
                   maybe_placeholders=False,
                   # Using an internal transformer is faster and more memory efficient
                   transformer=ASDocTransformer()
              )
parse = Parser.parse
#def parse(x):
#    return ASDocTransformer().transform(Parser.parse(x))
