{% macro getFunction(function) -%}
{{ function.split('(')[0].split(' ')[-1] }}
{%- endmacro %}
{% macro getVariable(variable) -%}
{{ variable.split(' ')[-1] }}
{%- endmacro %}
{% macro fixNewline(string) -%}
{{ string.replace('\\n', '\n *\n * ') }}
{%- endmacro %}
{% macro fixNewlineEnum(string) -%}
{{ string.replace('\\n', ' ') }}
{%- endmacro %}