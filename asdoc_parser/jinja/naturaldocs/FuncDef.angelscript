{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Common.angelscript" as Common %}
{% set _funcdefName = Common.getFunction(root.Name) %}
{% set _funcdefNamespace = root.Name | replace( _funcdefName + "(", Namespace.prefixNamespaceNoGlobal(root.Namespace) + _funcdefName + "(", 1 ) %}
/*
 * typedef: {{ _funcdefName }}
 * --- C++
 * funcdef {{ _funcdefNamespace }};
 * ---
 * {{ Common.fixNewline( root.Documentation ) }}
 */