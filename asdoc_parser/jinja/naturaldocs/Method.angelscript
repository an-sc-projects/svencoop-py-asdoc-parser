{% import "Common.angelscript" as Common %}
{% macro getMethod(method) -%}
{% set _methodName = Common.getFunction(method.Declaration) %}
/*
 * method: {{ _methodName }}
 * {{ Common.fixNewline( method.Documentation ) }}
 */
{{ method.Declaration }} {}
{%- endmacro %}
{% macro getMethods(array) -%}
{% for block in array|reverse %}
{{ getMethod( block.Method ) }}
{% endfor %}
{%- endmacro %}