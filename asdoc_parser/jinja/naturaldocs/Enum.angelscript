{% import "Namespace.angelscript" as Namespace %}{{Namespace.getNamespace(root.Namespace)}}
{% import "Common.angelscript" as Common %}
{% set _enumName = root.Name %}
{% set _enumNamespace = Namespace.prefixNamespaceNoGlobal(root.Namespace) + _enumName %}
/*
 * enum: {{ _enumName }}
 * --- C++
 * enum {{ _enumNamespace }} {...}
 * ---
 * {{ Common.fixNewline( root.Documentation ) }}
 *
 * Values:
 * {% for block in root.Values|reverse %}
 * {{ block.Value.Name }} - ({{ block.Value.Value }}) {{ Common.fixNewlineEnum( block.Value.Documentation ) }}{% endfor %}
 */